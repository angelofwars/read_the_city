import { createRouter, createWebHistory } from 'vue-router'
import TheBaseBase from '../views/TheBase.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: TheBaseBase
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
